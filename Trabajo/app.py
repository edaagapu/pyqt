# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(364, 208)
        MainWindow.setStyleSheet("border-color: qconicalgradient(cx:0.5, cy:0.5, angle:0, stop:0 rgba(35, 40, 3, 255), stop:0.16 rgba(136, 106, 22, 255), stop:0.225 rgba(166, 140, 41, 255), stop:0.285 rgba(204, 181, 74, 255), stop:0.345 rgba(235, 219, 102, 255), stop:0.415 rgba(245, 236, 112, 255), stop:0.52 rgba(209, 190, 76, 255), stop:0.57 rgba(187, 156, 51, 255), stop:0.635 rgba(168, 142, 42, 255), stop:0.695 rgba(202, 174, 68, 255), stop:0.75 rgba(218, 202, 86, 255), stop:0.815 rgba(208, 187, 73, 255), stop:0.88 rgba(187, 156, 51, 255), stop:0.935 rgba(137, 108, 26, 255), stop:1 rgba(35, 40, 3, 255));\n"
"background-color: rgb(255, 255, 255);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btnExcel = QtWidgets.QPushButton(self.centralwidget)
        self.btnExcel.setGeometry(QtCore.QRect(20, 120, 80, 25))
        self.btnExcel.setObjectName("btnExcel")
        self.btnOracle = QtWidgets.QPushButton(self.centralwidget)
        self.btnOracle.setGeometry(QtCore.QRect(120, 120, 80, 25))
        self.btnOracle.setObjectName("btnOracle")
        self.btnManual = QtWidgets.QPushButton(self.centralwidget)
        self.btnManual.setGeometry(QtCore.QRect(220, 120, 80, 25))
        self.btnManual.setObjectName("btnManual")
        self.cmbxX = QtWidgets.QComboBox(self.centralwidget)
        self.cmbxX.setGeometry(QtCore.QRect(40, 30, 161, 21))
        self.cmbxX.setObjectName("cmbxX")
        self.cmbxY = QtWidgets.QComboBox(self.centralwidget)
        self.cmbxY.setGeometry(QtCore.QRect(40, 70, 161, 25))
        self.cmbxY.setObjectName("cmbxY")
        self.btnGraficar = QtWidgets.QPushButton(self.centralwidget)
        self.btnGraficar.setGeometry(QtCore.QRect(230, 50, 80, 25))
        self.btnGraficar.setObjectName("btnGraficar")
        MainWindow.setCentralWidget(self.centralwidget)
        self.mbBarra = QtWidgets.QMenuBar(MainWindow)
        self.mbBarra.setGeometry(QtCore.QRect(0, 0, 364, 22))
        self.mbBarra.setObjectName("mbBarra")
        self.mnArchivo = QtWidgets.QMenu(self.mbBarra)
        self.mnArchivo.setObjectName("mnArchivo")
        self.mnAyuda = QtWidgets.QMenu(self.mbBarra)
        self.mnAyuda.setObjectName("mnAyuda")
        MainWindow.setMenuBar(self.mbBarra)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actAcerca = QtWidgets.QAction(MainWindow)
        self.actAcerca.setObjectName("actAcerca")
        self.actSalir = QtWidgets.QAction(MainWindow)
        self.actSalir.setObjectName("actSalir")
        self.mnArchivo.addAction(self.actSalir)
        self.mnAyuda.addAction(self.actAcerca)
        self.mbBarra.addAction(self.mnArchivo.menuAction())
        self.mbBarra.addAction(self.mnAyuda.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Ventana"))
        self.btnExcel.setText(_translate("MainWindow", "Excel"))
        self.btnOracle.setText(_translate("MainWindow", "Oracle"))
        self.btnManual.setText(_translate("MainWindow", "Manual"))
        self.btnGraficar.setText(_translate("MainWindow", "Graficar"))
        self.mnArchivo.setTitle(_translate("MainWindow", "Archivo"))
        self.mnAyuda.setTitle(_translate("MainWindow", "Ayuda"))
        self.actAcerca.setText(_translate("MainWindow", "Acerca de"))
        self.actSalir.setText(_translate("MainWindow", "Salir"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

