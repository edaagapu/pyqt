#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 21 01:48:15 2019

@author: edaagapu
"""

#Implementando interfaz con ayuda de QtDesigner.

import sys
import pandas as pd
#Importamos la ventana generada en QtDesigner
from app import *
from recolectarDatos import *
#Usando a chaco

from numpy import linspace, pi, sin
import matplotlib.pyplot as pl

class MiApp(QtWidgets.QMainWindow, Ui_MainWindow):
    
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setupUi(self)
        self.btnExcel.clicked.connect(self.impExcel)
        self.btnGraficar.clicked.connect(self.graficar)
        self.btnManual.clicked.connect(self.insertarDatos)
        self.arreglo = []
        
    def impExcel(self):
        rutaArchivo = QtWidgets.QFileDialog.getOpenFileName(self,"Open File","/home/edaagapu","Documentos de Excel (*.xls *.xlsx) ;; Hoja de Cálculo Open Office (*.ods)")
        self.archivo = pd.ExcelFile(str(rutaArchivo[0]))
        self.df = self.archivo.parse(self.archivo.sheet_names[0])
        if (self.cmbxX.count()>0):
            self.cmbxX.clear()
        if (self.cmbxY.count()>0):
            self.cmbxY.clear()    
        self.cmbxX.addItems(list(self.df.columns.values))
        self.cmbxY.addItems(list(self.df.columns.values))
        
    def graficar(self):
        ejeX = self.df[self.cmbxX.currentText()]
        ejeY = self.df[self.cmbxY.currentText()]
        pl.plot(ejeX,ejeY,"y")
        pl.show()
        
    def insertarDatos(self):
        self.ventana = QtWidgets.QMainWindow()
        self.ui = Ui_OtherWindow()
        self.ui.setupUi(self.ventana)
        self.ventana.show()
        self.ui.btnAgregar.clicked.connect(self.agregarDato)
        if (self.cmbxX.count()>0):
            self.cmbxX.clear()
        if (self.cmbxY.count()>0):
            self.cmbxY.clear()    
        self.cmbxX.addItems(["X","Y","Fecha","Lugar"])
        self.cmbxY.addItems(["X","Y","Fecha","Lugar"])
        
    def agregarDato(self):
        self.arreglo.append([self.ui.txtX.text(),self.ui.txtY.text(),self.ui.txtFecha.text(),self.ui.txtLugar.text()])
        print(self.arreglo)
        
if __name__=="__main__":
    app = QtWidgets.QApplication([])
    window = MiApp()
    window.show()
    app.exec()
