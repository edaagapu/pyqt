# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'graficar.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_AuxWindow(object):
    def setupUi(self, AuxWindow):
        AuxWindow.setObjectName("AuxWindow")
        AuxWindow.resize(421, 272)
        self.centralwidget = QtWidgets.QWidget(AuxWindow)
        self.centralwidget.setObjectName("centralwidget")
        AuxWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(AuxWindow)
        self.statusbar.setObjectName("statusbar")
        AuxWindow.setStatusBar(self.statusbar)

        self.retranslateUi(AuxWindow)
        QtCore.QMetaObject.connectSlotsByName(AuxWindow)

    def retranslateUi(self, AuxWindow):
        _translate = QtCore.QCoreApplication.translate
        AuxWindow.setWindowTitle(_translate("AuxWindow", "MainWindow"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AuxWindow = QtWidgets.QMainWindow()
    ui = Ui_AuxWindow()
    ui.setupUi(AuxWindow)
    AuxWindow.show()
    sys.exit(app.exec_())

