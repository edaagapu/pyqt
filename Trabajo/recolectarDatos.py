# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'recolectarDatos.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_OtherWindow(object):
    def setupUi(self, OtherWindow):
        OtherWindow.setObjectName("OtherWindow")
        OtherWindow.resize(345, 324)
        OtherWindow.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.centralwidget = QtWidgets.QWidget(OtherWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btnSalir = QtWidgets.QPushButton(self.centralwidget)
        self.btnSalir.setGeometry(QtCore.QRect(220, 260, 80, 25))
        self.btnSalir.setStyleSheet("border-color: rgb(84, 97, 0);")
        self.btnSalir.setObjectName("btnSalir")
        self.btnAgregar = QtWidgets.QPushButton(self.centralwidget)
        self.btnAgregar.setGeometry(QtCore.QRect(40, 260, 80, 25))
        self.btnAgregar.setStyleSheet("border-color: rgb(84, 97, 0);")
        self.btnAgregar.setObjectName("btnAgregar")
        self.txtX = QtWidgets.QLineEdit(self.centralwidget)
        self.txtX.setGeometry(QtCore.QRect(180, 30, 113, 25))
        self.txtX.setObjectName("txtX")
        self.txtY = QtWidgets.QLineEdit(self.centralwidget)
        self.txtY.setGeometry(QtCore.QRect(180, 70, 113, 25))
        self.txtY.setObjectName("txtY")
        self.txtFecha = QtWidgets.QLineEdit(self.centralwidget)
        self.txtFecha.setGeometry(QtCore.QRect(180, 110, 113, 25))
        self.txtFecha.setObjectName("txtFecha")
        self.lblX = QtWidgets.QLabel(self.centralwidget)
        self.lblX.setGeometry(QtCore.QRect(40, 30, 120, 25))
        self.lblX.setStyleSheet("font: 75 10pt \"Bitstream Vera Sans\";")
        self.lblX.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblX.setObjectName("lblX")
        self.txtLugar = QtWidgets.QLineEdit(self.centralwidget)
        self.txtLugar.setGeometry(QtCore.QRect(180, 150, 113, 25))
        self.txtLugar.setObjectName("txtLugar")
        self.lblY = QtWidgets.QLabel(self.centralwidget)
        self.lblY.setGeometry(QtCore.QRect(40, 70, 120, 25))
        self.lblY.setStyleSheet("font: 75 10pt \"Bitstream Vera Sans\";")
        self.lblY.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblY.setObjectName("lblY")
        self.lblLugar = QtWidgets.QLabel(self.centralwidget)
        self.lblLugar.setGeometry(QtCore.QRect(40, 150, 120, 25))
        self.lblLugar.setStyleSheet("font: 75 10pt \"Bitstream Vera Sans\";")
        self.lblLugar.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblLugar.setObjectName("lblLugar")
        self.lblFecha = QtWidgets.QLabel(self.centralwidget)
        self.lblFecha.setGeometry(QtCore.QRect(40, 110, 120, 25))
        self.lblFecha.setStyleSheet("font: 75 10pt \"Bitstream Vera Sans\";")
        self.lblFecha.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblFecha.setObjectName("lblFecha")
        OtherWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(OtherWindow)
        self.statusbar.setObjectName("statusbar")
        OtherWindow.setStatusBar(self.statusbar)

        self.retranslateUi(OtherWindow)
        QtCore.QMetaObject.connectSlotsByName(OtherWindow)

    def retranslateUi(self, OtherWindow):
        _translate = QtCore.QCoreApplication.translate
        OtherWindow.setWindowTitle(_translate("OtherWindow", "Recolectar Datos"))
        self.btnSalir.setText(_translate("OtherWindow", "Salir"))
        self.btnAgregar.setText(_translate("OtherWindow", "Agregar"))
        self.lblX.setText(_translate("OtherWindow", "X:"))
        self.lblY.setText(_translate("OtherWindow", "Y:"))
        self.lblLugar.setText(_translate("OtherWindow", "Lugar:"))
        self.lblFecha.setText(_translate("OtherWindow", "Fecha:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    OtherWindow = QtWidgets.QMainWindow()
    ui = Ui_OtherWindow()
    ui.setupUi(OtherWindow)
    OtherWindow.show()
    sys.exit(app.exec_())

